#include<stdio.h> 
#include<malloc.h> 
#include<string.h>
#include<stdlib.h>
#define stacksize 100 


typedef struct { 
	char *data; 
	int top; 
} sqstack; 

int initstack(sqstack *S)
{ 
	S->top=0;
	return 0;
} 
 
void push(sqstack *S,char x)
{ 
	if(S->top==stacksize){
		printf("stack overflow\n");
	} 
	else{
		S->data[S->top]=x;
		S->top=S->top+1;
	}
} 

int pop(sqstack *S)
{ 
	if(S->top==0) printf("stack empty");
	else
		return S->data[--S->top];
} 

int gettop(sqstack *S)
{
	if(S->top==0) printf("stack empty");
	else
		return S->data[S->top-1];
}

int symmetry(char *str)
{
	sqstack *S;
	int j,k,i=0;
	S=(sqstack*)malloc(sizeof(sqstack));
	S->data=(char*)malloc(sizeof(char)*stacksize);
	initstack(S);
	while(str[i]!='\0') i++;
	for(j=0;j<i/2;j++) push(S,str[j]);
	if(i%2!=0) k=(i+1)/2;
	else k=i/2;
	for(j=k;j<i;j++){
		if(str[j]!=pop(S)){
			free(S->data);
			free(S);
			return 0;
		}
	}
	free(S->data);
	free(S);
	return 1;
}

void main()
{
	char str[100];
	printf("Input:\n");
	scanf("%s",&str);
	if(symmetry(str))
		printf("symmetry\n");
	else
		printf("not symmetry\n");
	system("pause");
}