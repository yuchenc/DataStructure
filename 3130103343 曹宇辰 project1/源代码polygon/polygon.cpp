#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>
#include "misc.h"
#include "vecmtx.h"
#include "polygonprocess.h"


void myinit(void)
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glShadeModel(GL_FLAT);
}

void CALLBACK display(void)
{
	printf("display!\n");
}


void pointsdisp(int n, Point3D *Pl)
{
	int i;
	float cx, cy, cz;
	
	
	// display a set of points
	glPointSize(5.0);
	
	glBegin(GL_POINTS);
	for (i = 0; i < n; i++) {
		cx = (float)(Pl[i].x);
		cy = (float)(Pl[i].y);
		cz = (float)(Pl[i].z);
		glVertex3f(cx,cy,cz);
	}
	glEnd();
	glFlush();
}

void polygondisp(int n, Point3D *Pl, int closed)
{
	int i;
	float cx, cy, cz;

	//display the polygon
	//glLineWidth(4.0);
	glBegin(GL_LINE_STRIP);
	
	for (i = 0; i < n; i++) {
		cx = (float)(Pl[i].x);
		cy = (float)(Pl[i].y);
		cz = (float)(Pl[i].z);
		glVertex3f(cx,cy,cz);
	}
	if (closed)	{
	cx = (float)(Pl[0].x);
	cy = (float)(Pl[0].y);
	cz = (float)(Pl[0].z);
	glVertex3f(cx,cy,cz);
	}

	double d, d0;
	int j;
	int no1, no2;
	d = 0;
	for (i=0; i<n ;i++){
		for (j=i+1; j<n ;j++){
			d0 = pow(Pl[i].x-Pl[j].x,2) + pow(Pl[i].y-Pl[j].y,2) + pow(Pl[i].z-Pl[j].z,2);
			if (d0 > d){
				d=d0;
				no1=i;
				no2=j;
			}
		}
	}
	glColor3f(1.0,1.0,1.0);
	cx = (float)(Pl[no1].x);
	cy = (float)(Pl[no1].y);
	cz = (float)(Pl[no1].z);
	glVertex3f(cx,cy,cz);
	glColor3f(1.0,0.0,1.0);
	cx = (float)(Pl[no2].x);
	cy = (float)(Pl[no2].y);
	cz = (float)(Pl[no2].z);
	glVertex3f(cx,cy,cz);

	glEnd();
	glFlush();


}

void	DisplayPolygon3D(Polygon3D *plyg)
{
	if (plyg->num_pnt<=0)	return;

	int i;
	float cx, cy, cz;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.0, 0.0, 1.0);
	polygondisp(plyg->num_pnt, plyg->Point, plyg->closed);

	glColor3f(1.0, 0.0, 0.0);
	pointsdisp(plyg->num_pnt, plyg->Point);
}

void main(int argc, char** argv)
{
	char filename[12];
    MSG msg;

	if (argc==1)  {
		printf("Please input the data file:\n");
		scanf("%s", filename);
	} 

	Polygon3D *plyg;

	if (argc==1) plyg = (Polygon3D *)InputPolygon(filename);
	else		 plyg = (Polygon3D *)InputPolygon(argv[1]);

	double	t0, t1, time;
	t0=GetTickCount();
	BoundingcomputationForPolygon(plyg);
	t1=GetTickCount();
	time=(t1-t0)/1000.0;
	printf("time=%lf\n",time);

	auxInitDisplayMode (AUX_SINGLE | AUX_RGBA);
	auxInitPosition (0, 0, 700, 700);
	auxInitWindow ("display polygon");
	myinit();
			


	while (1) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) == FALSE)
			DisplayPolygon3D(plyg);
		if (GetMessage(&msg, NULL, 0, 0) != TRUE) {
			break;
    } 
    TranslateMessage(&msg);
    DispatchMessage(&msg);
    }

	auxMainLoop(display);
}
