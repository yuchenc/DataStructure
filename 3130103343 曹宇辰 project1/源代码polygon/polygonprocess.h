
#ifndef    CURVEFILTER_H
#define    CURVEFILTER_H

#include "vecmtx.h"

typedef struct {
	int		num_pnt;	// the number of points
    Point3D *Point;		// the point array
	int		closed;		// 1=closed 0=open
} Polygon3D;



Polygon3D *InputPolygon(char filename[]);

void BoundingcomputationForPolygon(Polygon3D *plyg);


void InputData(char filename[], int *n, Point3D *P);

void OutputData(char *filename, int n, Point3D *P);

void SavePointData(char *filename, int n, Point3D *P, int dpth);

void SaveList(char *filename, int n, Real *list);

void Boundingcomputation(int n, Point3D *Pl);

void	PolygonVertexReodering(int n, Point3D *P);

void WorldAngleComputation(int n, Point3D *Q, Real *ag);




#endif