
/***********************************************************\
*                                                           *
*   Operators on vectors & matrixes                         *
*                                                           *
\***********************************************************/
#include "misc.h"
#include "vecmtx.h"


int   RealZero (Real re, Real eps)   /* Is |re| <= eps ? */
{
    if (re <= eps && re >= -eps)  return  TRUE;
    else   return  FALSE;
}

/*char  *MyMalloc (unsigned bytes)
{
	char *tmp = (char *)malloc(bytes);
    if (!tmp) {
        fprintf (stderr, "\n Space overflowed !!!");
        exit (-1);
    }
    return tmp;
}*/



/**********************************************************\
*                                                          *
*          Operations for 3D Vectors                       *
*                                                          *
\**********************************************************/

void  CopyVec3D (Vector3D *newv, Vector3D *old)      /* newv = old */
{
    newv->x = old->x;
    newv->y = old->y;
    newv->z = old->z;
}


void  SetVec3D (Vector3D *v, Real x, Real y, Real z)     /*  v = (x, y, z)  */
{
    v->x = x;    v->y = y;   v->z = z;
}

void  SetVec4D (Vector4D *v, Real x, Real y, Real z, Real w)     /*  v = (x, y, z, w)  */
{
    v->x = x;    v->y = y;   v->z = z;  v->w = w;
}

void  AddVec3D (Vector3D *v, Vector3D *v1, Vector3D *v2)      /* v = v1 + v2  */
{
    v->x = v1->x + v2->x;
    v->y = v1->y + v2->y;
    v->z = v1->z + v2->z;
}


void  SubVec3D (Vector3D *v, Vector3D *v1, Vector3D *v2)       /* v = v1 - v2  */
{
    v->x = v1->x - v2->x;
    v->y = v1->y - v2->y;
    v->z = v1->z - v2->z;
}


void  ScaleVec3D (Vector3D *v, Real coef, Vector3D *v1)   /*  v = coef * v1  */
{
    v->x = coef * v1->x;
    v->y = coef * v1->y;
    v->z = coef * v1->z;
}


int   Vec3DOverScale (Vector3D *v, Vector3D *v1, Real dm)  /* v = v1 / dm  */
{
    if (RealZero (dm, ZeroEps)) return FAIL;
    v->x = v1->x/dm;
    v->y = v1->y/dm;
    v->z = v1->z/dm;
    return  TRUE;
}


void  CombVec3D (Vector3D *v, Real c1, Vector3D *v1, Real c2, Vector3D *v2)  /* v = c1*v1 + c2*v2 */
{
    v->x = c1*v1->x + c2*v2->x;
    v->y = c1*v1->y + c2*v2->y;
    v->z = c1*v1->z + c2*v2->z;
}


void  DotVec3D (Real *dot, Vector3D *v1, Vector3D *v2)     /*  dot = v1 * v2  */
{
    *dot = v1->x*v2->x + v1->y*v2->y + v1->z*v2->z;
}


void  CrossVec3D (Vector3D *v, Vector3D *v1, Vector3D *v2)    /*  v = v1 x v2   */
{
    v->x = v1->y*v2->z - v1->z*v2->y;
    v->y = v1->z*v2->x - v1->x*v2->z;
    v->z = v1->x*v2->y - v1->y*v2->x;
}


int   LenVec3D (Real *len, Vector3D *v)         /* len = |v| */
{
    Real    a, b, c;

    a = fabs (v->x);   b = fabs (v->y);   c = fabs (v->z);
    if (a >= b && a >=c) {
        if (RealZero (a, ZeroEps))  *len = 0.0;
        else {
            b /= a;   c /= a;
            *len = a * sqrt (1.0 + b*b + c*c);
        }
    }
    else if (b >= a && b >= c) {
        if (RealZero (b, ZeroEps))  *len = 0.0;
        else {
            a /= b;   c /= b;
            *len = b * sqrt (1.0 + a*a + c*c);
        }
    }
    else {
        if (RealZero (c, ZeroEps))  *len = 0.0;
        else {
            a /= c;   b /= c;
            *len = c * sqrt (1.0 + a*a + b*b);
        }
    }

    if (RealZero (*len, ZeroEps))  return  FAIL;
    return  TRUE;
}



int   DistVec3D (Real *len, Vector3D *v1, Vector3D *v2)
{
    Point3D  v;

    SubVec3D (&v, v1, v2);
    return  LenVec3D (len, &v);
}


int   UnitVec3D (Vector3D *v)     /* v = v/|v|   */
{
    Real    len;

    LenVec3D (&len, v);
    if (RealZero (len, ZeroEps))  return FAIL;
    v->x /= len;
    v->y /= len;
    v->z /= len;
    return  TRUE;
}


int   ZeroVec3D (Vector3D *v)    /* Is v zerovector ?  */
{
    if (RealZero (fabs (v->x)+fabs (v->y)+fabs (v->z), ZeroEps))
              return (TRUE);
    else    return (FALSE);
}


int   SameVec3D (Vector3D *v1, Vector3D *v2)    /* v1 == v2 ?  */
{
    Vector3D  v;
    SubVec3D (&v, v1, v2);
    if (ZeroVec3D (&v))   return (TRUE);
    else    return (FALSE);
}


int   SamePnt3D (Vector3D *v1, Vector3D *v2)    /* v1 == v2 ?  */
{
    if (fabs (v1->x - v2->x) + fabs (v1->y - v2->y) +
        fabs (v1->z - v2->z) < SptEps)  return  TRUE;
    return   FALSE;
}




/**********************************************************\
*                                                          *
*          Operations for 4D Vectors                       *
*                                                          *
\**********************************************************/

void  CopyVec4D (Vector4D *newv, Vector4D *old)      /* new = old */
{
    newv->x = old->x;
    newv->y = old->y;
    newv->z = old->z;
    newv->w = old->w;
}


void  AddVec4D (Vector4D *v, Vector4D *v1, Vector4D *v2)     /* v = v1 + v2  */
{
    v->x = v1->x + v2->x;
    v->y = v1->y + v2->y;
    v->z = v1->z + v2->z;
    v->w = v1->w + v2->w;
}

int  LenVec4D (Real *len, Vector4D *v)
{
    Real  a, b, c, d, t;

    a = v->x;     b = v->y;
    c = v->z;     d = v->w;

    t     = a*a + b*b + c*c + d*d;
    *len  = sqrt(t);

    return  TRUE;
}


int  UnitVec4D (Vector4D *v)              /* v = v/|v|  */
{
    Real   len;

    LenVec4D (&len, v);
    if (RealZero (len, ZeroEps))  return FAIL;
    v->x /= len;
    v->y /= len;
    v->z /= len;
    v->w /= len;
    return  TRUE;
}

void  SubVec4D (Vector4D *v, Vector4D *v1, Vector4D *v2)       /* v = v1 - v2  */
{
    v->x = v1->x - v2->x;
    v->y = v1->y - v2->y;
    v->z = v1->z - v2->z;
    v->w = v1->w - v2->w;
}

void  ScaleVec4D (Vector4D *v, Real coef, Vector4D *v1)   /*  v = coef * v1  */
{
    v->x = coef * v1->x;
    v->y = coef * v1->y;
    v->z = coef * v1->z;
    v->w = coef * v1->w;
}


void  CombVec4D (Vector4D *v, Real c1, Vector4D *v1, Real c2, Vector4D *v2)  /* v = c1*v1 + c2*v2 */
{
    v->x = c1*v1->x + c2*v2->x;
    v->y = c1*v1->y + c2*v2->y;
    v->z = c1*v1->z + c2*v2->z;
    v->w = c1*v1->w + c2*v2->w;
}


void  Vec3To4 (Vector4D *v4, Vector3D *v3, Real w)  /* v4 = (v3,1)*w */
{
    v4->x = v3->x * w;
    v4->y = v3->y * w;
    v4->z = v3->z * w;
    v4->w = w;
}


void  Vec4Trunc3 (Vector3D *v3, Vector4D *v4)  /* v3 = (x, y, z) of v4 */
{
    v3->x = v4->x;
    v3->y = v4->y;
    v3->z = v4->z;
}


int   Vec4Prj3 (Vector3D *v3, Vector4D *v4)  /* v3 = (x,y,z)/w of v4 */
{
    if (RealZero (v4->w, ZeroEps))  return  FAIL;
    v3->x = v4->x / v4->w;
    v3->y = v4->y / v4->w;
    v3->z = v4->z / v4->w;
    return  TRUE;
}



/**********************************************************\
*                                                          *
*          Operations for 3D Matrices                      *
*                                                          *
\**********************************************************/

void  InitMtx3D (Matrix3D mtx)    /* mtx = I */
{
    int    i, j;
    for (i=0; i<4; i++)
        for (j=0; j<4; j++)
            mtx[i][j] =  (i==j)?1.0:0.0;
}



void  ProperView (Matrix3D mv)    /* one matrix for a proper view */
{
    int      i;

    mv[0][0]= 0.8728715; mv[0][1]= -0.4364357; mv[0][2]= -0.2182178;
    mv[1][0]= 0.2672612; mv[1][1]=  0.8017837; mv[1][2]= -0.5345224;
    mv[2][0]= 0.4082482; mv[2][1]=  0.4082482; mv[2][2]=  0.8164965;
    
    for (i=0;i<3;i++)  mv[3][i] = mv[i][3] = 0.0; 
    mv[3][3]=1.0;
}


void  CopyMtx3D (Matrix3D newm, Matrix3D old)    /* new = old */
{
    int    i, j;
    for (i=0; i<4; i++)
        for (j=0; j<4; j++)
            newm[i][j] = old[i][j];
}


void  MulMtx3D (Matrix3D mtx, Matrix3D mtx1, Matrix3D mtx2)   /* mtx = mtx1 * mtx2 */
{
    int        i, j, k;
    Matrix3D   mtx3;   
   
    for (i=0; i<4; i++)  
        for (j=0; j<4; j++) {  
            mtx3[i][j] = 0.0;
            for (k=0; k<4; k++)  
                mtx3[i][j] += mtx1[i][k]*mtx2[k][j] ; 
        }
    CopyMtx3D (mtx, mtx3);
}




/**********************************************************\
*                                                          *
*         Some procedures for transformation               *
*                                                          *
\**********************************************************/

void  ShiftMtx3D (Matrix3D mtx, Real x, Real y, Real z)  /* translate with (x, y, z) */
{
    int     i, j;
    Real    d[3];
 
    d[0] = x;   d[1] = y;   d[2] = z;
         
    for (i=0; i<3; i++) {
        for (j=0; j<3; j++)   mtx[i][j] += mtx[i][3]*d[j]; 
        mtx[3][i] += mtx[3][3]*d[i];
    }
}


void  ScaleMtx3D (Matrix3D mtx, Real s)  /* scale with s */
{
    int     i, j;
        
    for (i=0; i<4; i++) 
        for (j=0; j<3; j++)
            mtx[i][j] *= s; 
}
            
       
 
                     
/* 
** to rotate an object around n-axis with angle (degree).
**    n = 0 :   x axis
**    n = 1 :   y axis
**    n = 2 :   z axis
*/ 
void  RotNMtx3D (Matrix3D mtx, Real angle, int n) 
{
    Matrix3D  mrot, mtx1;
    Real      ang;

    ang = angle*DTOR;
    InitMtx3D (mrot);
    
    mrot[(n+1)%3][(n+1)%3] = mrot[(n+2)%3][(n+2)%3] = cos (ang);
    mrot[(n+1)%3][(n+2)%3] =  sin (ang);
    mrot[(n+2)%3][(n+1)%3] = -sin (ang);

    MulMtx3D (mtx1, mtx, mrot);
    CopyMtx3D (mtx, mtx1);
}


void  RotXMtx3D (Matrix3D mtx, Real Angle)   /* rotate around x axis */
{
    RotNMtx3D (mtx, Angle, 0);
}


void  RotYMtx3D (Matrix3D mtx, Real Angle)   /* rotate around y axis */
{
    RotNMtx3D (mtx, Angle, 1);
}

void  RotZMtx3D (Matrix3D mtx, Real Angle)   /* rotate around z axis */
{
    RotNMtx3D (mtx, Angle, 2);
}
       

int   TransfVec3D (Vector3D *newv, Vector3D *p, Matrix3D mtx)   /*  new = p*mtx  */
{
    Vector3D  pp;
    Real      w;

    w = p->x * mtx[0][3] + p->y * mtx[1][3] + 
        p->z * mtx[2][3] + mtx[3][3];
    if (RealZero (w, ZeroEps))   return  FAIL;

    pp.x = p->x * mtx[0][0] + p->y * mtx[1][0] + 
           p->z * mtx[2][0] + mtx[3][0];
    pp.y = p->x * mtx[0][1] + p->y * mtx[1][1] + 
           p->z * mtx[2][1] + mtx[3][1];
    pp.z = p->x * mtx[0][2] + p->y * mtx[1][2] + 
           p->z * mtx[2][2] + mtx[3][2];

    newv->x = pp.x/w;
    newv->y = pp.y/w;
    newv->z = pp.z/w;
    return  TRUE;
}


/*
** to compute the inverse matrix of a known matrix
** mti: the inverse matrix 
** mtx: the original matrix 
*/
int   MatrixInv(Matrix2D mti, Matrix2D mtx)
{
	Real   vm;
	Real   l11, l12, l13;
	Real   l21, l22, l23;
	Real   l31, l32, l33;

	l11 = mtx[1][1]*mtx[2][2] - mtx[2][1]*mtx[1][2];
	l12 = mtx[2][1]*mtx[0][2] - mtx[0][1]*mtx[2][2];
	l13 = mtx[0][1]*mtx[1][2] - mtx[1][1]*mtx[0][2];

	l21 = mtx[2][0]*mtx[1][2] - mtx[1][0]*mtx[2][2];
	l22 = mtx[0][0]*mtx[2][2] - mtx[2][0]*mtx[0][2];
	l23 = mtx[1][0]*mtx[0][2] - mtx[0][0]*mtx[1][2];

	l31 = mtx[1][0]*mtx[2][1] - mtx[2][0]*mtx[1][1];
	l32 = mtx[2][0]*mtx[0][1] - mtx[0][0]*mtx[2][1];
	l33 = mtx[0][0]*mtx[1][1] - mtx[1][0]*mtx[0][1];

	vm  = mtx[0][0]*l11 + mtx[1][0]*l12 + mtx[2][0]*l13;
	if (vm<0.000001&&vm>-0.000001)   return  FAIL;

	mti[0][0] = l11/vm;
	mti[0][1] = l12/vm;
	mti[0][2] = l13/vm;
	mti[1][0] = l21/vm;
	mti[1][1] = l22/vm;
	mti[1][2] = l23/vm;
	mti[2][0] = l31/vm;
	mti[2][1] = l32/vm;
	mti[2][2] = l33/vm;

	return   TRUE;
}
 
 
/* 
** to make a transform matrix from Local Coordinate
**    System to World Coordinate System
*/
int   LcsToWcs (Matrix3D mtx, Lcs3D *lcs)
{
    Vector3D    tmp;
    Real        lx, ly, lz;
    Matrix2D    mt2, mti;

    if (!LenVec3D (&lx, &(lcs->x_axis)))    return  FAIL;
    if (!LenVec3D (&ly, &(lcs->y_axis)))    return  FAIL;
    if (!LenVec3D (&lz, &(lcs->z_axis)))    return  FAIL;

    if (!RealZero (lx-1.0, ZeroEps) ||
        !RealZero (ly-1.0, ZeroEps) ||
        !RealZero (lz-1.0, ZeroEps) )     return  FAIL;

    CrossVec3D (&tmp, &(lcs->x_axis), &(lcs->y_axis));
    if (!SameVec3D (&tmp, &(lcs->z_axis)))  return  FAIL;

    mt2[0][0] = lcs->x_axis.x;
    mt2[0][1] = lcs->x_axis.y;
    mt2[0][2] = lcs->x_axis.z;
 
    mt2[1][0] = lcs->y_axis.x;
    mt2[1][1] = lcs->y_axis.y;
    mt2[1][2] = lcs->y_axis.z;
 
    mt2[2][0] = lcs->z_axis.x;
    mt2[2][1] = lcs->z_axis.y;
    mt2[2][2] = lcs->z_axis.z;
 
    if (!MatrixInv(mti, mt2))    return FAIL;
    mtx[0][0] = mt2[0][0];
    mtx[0][1] = mt2[0][1];
    mtx[0][2] = mt2[0][2];
    mtx[0][3] = 0.0;

    mtx[1][0] = mt2[1][0];
    mtx[1][1] = mt2[1][1];
    mtx[1][2] = mt2[1][2];
    mtx[1][3] = 0.0;

    mtx[2][0] = mt2[2][0];
    mtx[2][1] = mt2[2][1];
    mtx[2][2] = mt2[2][2];
    mtx[2][3] = 0.0;
 
    mtx[3][0] = lcs->org.x;
    mtx[3][1] = lcs->org.y;
    mtx[3][2] = lcs->org.z;
    mtx[3][3] = 1.0;
 
    return  TRUE;
}
 

 
/*
** to make a transform matrix from World Coordinate
**    System to Local Coordinate System
*/
int   WcsToLcs (Matrix3D mtx, Lcs3D *lcs)
{
    Vector3D    tmp;
    Real        lx, ly, lz;

    if (!LenVec3D (&lx, &(lcs->x_axis)))    return  FAIL;
    if (!LenVec3D (&ly, &(lcs->y_axis)))    return  FAIL;
    if (!LenVec3D (&lz, &(lcs->z_axis)))    return  FAIL;

    if (!RealZero (lx-1.0, ZeroEps) ||
        !RealZero (ly-1.0, ZeroEps) ||
        !RealZero (lz-1.0, ZeroEps) )     return  FAIL;

    CrossVec3D (&tmp, &(lcs->x_axis), &(lcs->y_axis));
    if (!SameVec3D (&tmp, &(lcs->z_axis)))  return  FAIL;

    mtx[0][0] = lcs->x_axis.x;
    mtx[0][1] = lcs->y_axis.x;
    mtx[0][2] = lcs->z_axis.x;
    mtx[0][3] = 0.0;
    
    mtx[1][0] = lcs->x_axis.y;
    mtx[1][1] = lcs->y_axis.y;
    mtx[1][2] = lcs->z_axis.y;
    mtx[1][3] = 0.0;
 
    mtx[2][0] = lcs->x_axis.z;
    mtx[2][1] = lcs->y_axis.z;
    mtx[2][2] = lcs->z_axis.z;
    mtx[2][3] = 0.0;
    
    mtx[3][0] = -(lcs->x_axis.x*lcs->org.x + lcs->x_axis.y*lcs->org.y 
                  + lcs->x_axis.z * lcs->org.z);
    mtx[3][1] = -(lcs->y_axis.x*lcs->org.x + lcs->y_axis.y*lcs->org.y 
                  + lcs->y_axis.z * lcs->org.z);
    mtx[3][2] = -(lcs->z_axis.x*lcs->org.x + lcs->z_axis.y*lcs->org.y 
                  + lcs->z_axis.z * lcs->org.z);
    mtx[3][3] = 1.0;  
 
    return  TRUE;
}
 



