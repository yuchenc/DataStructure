

#include <windows.h>
//#include <stdio.h>
//#include <math.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <GL/glaux.h>
#include "misc.h"
#include "vecmtx.h"
#include "polygonprocess.h"

float frand(void)
{
    return 1.0f*(rand()/32768.0f);  //0<frand()<1
}

Polygon3D *InputPolygon(char filename[])
{
	FILE   *fp;
	int    i, n, n0, cls=0;
	Polygon3D *plyg;
	Point3D *P;

	if((fp = fopen(filename, "r")) == NULL) exit(1);
	fscanf (fp, "%d", &n);

	n0 = n < 100 ? n : 100;
	if (n0>0)  {
		P = (Point3D *)malloc(n0 * sizeof (Point3D ));
	//	P = new Point3D[100];
	}
	else return (Polygon3D *)NULL;

	for (i=0; i<n0; i++)   {
	    fscanf (fp, "%lf%lf%lf",&(P[i].x), &(P[i].y), &(P[i].z));
		printf("P[%d]:%lf %lf %lf\n",i,P[i].x,P[i].y,P[i].z);
	}

	// realloc space when the initial space is insufficient
	if (n>100)	{
		P = (Point3D *)realloc(P, n * sizeof (Point3D ));
		for (i=100; i<n; i++)	{
			fscanf (fp, "%lf%lf%lf",&(P[i].x), &(P[i].y), &(P[i].z));
			printf("P[%d]:%lf %lf %lf\n",i,P[i].x,P[i].y,P[i].z);
		}
	}

	fscanf (fp, "%d", &cls);
	fclose (fp);

	plyg = (Polygon3D *)malloc(sizeof (Polygon3D ));
	plyg->num_pnt=n;
	plyg->Point=P;
	plyg->closed=(cls > 0)? 1 : 0;

	return plyg;
}

/*
 *	n the point number, P the point list, itn the iteration number
 */
void	InputData(char filename[], int *n, Point3D *P)
{
	FILE   *fp;
	int    i, n0;

	if((fp = fopen(filename, "r")) == NULL) exit(1);
	fscanf (fp, "%d", n);
	n0  = *n;

	if (n0>=1000)  {
		printf("Too many data points!\n");
		return;
	}

	for (i=0; i<n0; i++)   {
	    fscanf (fp, "%lf%lf%lf",&(P[i].x), &(P[i].y), &(P[i].z));
		printf("n0=%d P[%d]:%lf %lf %lf\n",n0,i,P[i].x,P[i].y,P[i].z);
	}
//	fscanf (fp, "%d", itn);
	fclose (fp);
}


void    OutputData(char *filename, int n, Point3D *P)
{
    FILE   *fp;
    int    i;

	if ((fp = fopen(filename, "w")) == NULL) return;
    fprintf (fp, "%d\n", n);

	fprintf(fp,"the x coordinates\n");
    for (i=0; i<n; i++)   
		fprintf (fp, "%lf\n",P[i].x);
	fprintf(fp,"the y coordinates\n");
    for (i=0; i<n; i++)   
		fprintf (fp, "%lf\n",P[i].y);

	fprintf(fp,"the entire coordinates\n");
	for (i=0; i<n; i++)   {
        fprintf (fp, "%lf %lf %lf\n",P[i].x, P[i].y, P[i].z);
    }
    fclose (fp);
}

void    SavePointData(char *filename, int n, Point3D *P, int dpth)
{
    FILE   *fp;
    int    i;

	if ((fp = fopen(filename, "w")) == NULL) return;
    fprintf (fp, "%d\n", n);  //the number of points

	for (i=0; i<n; i++)   {
        fprintf (fp, "%lf %lf %lf\n",P[i].x, P[i].y, P[i].z);
    }
	fprintf (fp, "%d\n", dpth);  //the dpth for iteration
	fprintf(fp,"the end\n");
    fclose (fp);
}

void SaveList(char *filename, int n, Real *list)
{
    FILE   *fp;
    int    i;

	if ((fp = fopen(filename, "w")) == NULL) return;
    fprintf (fp, "%d\n", n);

	fprintf(fp,"data list\n");
    for (i=0; i<n; i++)   
		fprintf (fp, "%lf\n",list[i]);
    fclose (fp);
}


void WorldAngleComputation(int n, Point3D *Q, Real *ag)
{
	int i;
	Vector3D Vd, Ux, Uy;
	Real	 ax, ay;

	SetVec3D (&Ux, 1.0, 0.0, 0.0);
	SetVec3D (&Uy, 0.0, 1.0, 0.0);
	ag[0] =0.0;
	for (i=1; i<n; i++) {
		SubVec3D (&Vd, &Q[i], &Q[i-1]);
		DotVec3D (&ax, &Vd, &Ux);
		DotVec3D (&ay, &Vd, &Uy);
		ag[i] = atan2(ay, ax); 
	}
}

/*
** Rescale a set of input data to fit a prescribed window
*/
void Boundingcomputation(int n, Point3D *Pl)
{
	int	i;
	Point3D box_center; //the center of bounding box
	Real	rad;		//the radiance of bounding box

	if (n<1) {
		SetVec3D (&box_center, 0.0, 0.0, 0.0);
		rad = 1.0;
		return;
	}

	Real xmin, xmax, ymin, ymax;
	xmin = 10000.0;
	ymin = 10000.0;
	xmax = -10000.0;
	ymax = -10000.0;
	for (i=0; i<n; i++) {
		if (Pl[i].x<xmin) xmin = Pl[i].x;
		if (Pl[i].x>xmax) xmax = Pl[i].x;
		if (Pl[i].y<ymin) ymin = Pl[i].y;
		if (Pl[i].y>ymax) ymax = Pl[i].y;
	}
	Real xb, yb; 
	xb = (xmin+xmax)/2.0;
	yb = (ymin+ymax)/2.0;
	SetVec3D(&box_center,xb,yb,0.0);

	xb  = (xmax-xmin)/2.0;
	yb  = (ymax-ymin)/2.0;
	rad = xb>yb ? xb : yb;
	rad = rad*1.2;

	Real cx, cy, cz;
	for (i = 0; i < n; i++) {
		cx = (Pl[i].x-box_center.x)/rad;
		cy = (Pl[i].y-box_center.y)/rad;
		cz = 0.0;
		SetVec3D(&Pl[i], cx,cy,cz);
	}
}

void BoundingcomputationForPolygon(Polygon3D *plyg)
{
	int	i, n;
	Point3D box_center; //the center of bounding box
	Real	rad;		//the radiance of bounding box

	n = plyg->num_pnt;
	if (n<1) {
		SetVec3D (&box_center, 0.0, 0.0, 0.0);
		rad = 1.0;
		return;
	}

	Real xmin, xmax, ymin, ymax;
	xmin = 10000.0;
	ymin = 10000.0;
	xmax = -10000.0;
	ymax = -10000.0;
	for (i=0; i<n; i++) {
		if (plyg->Point[i].x<xmin) xmin = plyg->Point[i].x;
		if (plyg->Point[i].x>xmax) xmax = plyg->Point[i].x;
		if (plyg->Point[i].y<ymin) ymin = plyg->Point[i].y;
		if (plyg->Point[i].y>ymax) ymax = plyg->Point[i].y;
	}
	Real xb, yb; 
	xb = (xmin+xmax)/2.0;
	yb = (ymin+ymax)/2.0;
	SetVec3D(&box_center,xb,yb,0.0);

	xb  = (xmax-xmin)/2.0;
	yb  = (ymax-ymin)/2.0;
	rad = xb>yb ? xb : yb;
	rad = rad*1.2;

	Real cx, cy, cz;
	for (i = 0; i < n; i++) {
		cx = (plyg->Point[i].x-box_center.x)/rad;
		cy = (plyg->Point[i].y-box_center.y)/rad;
		cz = 0.0;
		SetVec3D(&plyg->Point[i], cx,cy,cz);
	}
}



// order change for a polygon curve
void	PolygonVertexReodering(int n, Point3D *P)
{
	int i, step;
	Point3D Pt;

	step = 3;
	if (n<=2)  return;
	for (i=2; i<(n-step); i+=step)  {
		CopyVec3D (&Pt, &P[i]);
		CopyVec3D (&P[i], &P[i+step-1]);
		CopyVec3D (&P[i+step-1], &Pt);
	}
}




