#ifndef    VECMTX_H
#define    VECMTX_H

#include   <stdio.h>
#include   <math.h>

#ifndef  SINGLE_PRECISION
#define  Real double
#else 
#define  Real float
#endif

      

/*
**   Data Structure for Vector and Matrix 
*/

typedef Real  Matrix2D[3][3];
typedef Real  Matrix3D[4][4];


typedef struct  {
    Real        x, y;
} Point2D;


typedef struct  {
    Real        x, y, z;
} Point3D;


typedef struct {         
    Real       x, y, z, w;
}   Point4D;


typedef    Point2D    Vector2D;
typedef    Point3D    Vector3D;
typedef    Point4D    Vector4D;


typedef struct {  
    Point2D         org;
    Vector2D        dir;
} Axis2D;


typedef struct {
    Point3D         org;
    Vector3D        dir;
} Axis3D;


typedef struct { /* Cartesian 2D local coordinate system */
    Point2D         org;
    Vector2D        x_axis ;
    Vector2D        y_axis ;
} Lcs2D;


typedef struct { /* Cartesian 3D local coordinate system */
    Point3D         org;
    Vector3D        x_axis ;
    Vector3D        y_axis ;
    Vector3D        z_axis ;
} Lcs3D;


/*
**   Common Procedures for Vector and Matrix Operations
*/

int   RealZero (Real re, Real eps);   /* Is |re| <= eps ? */

void  CopyVec3D (Vector3D *newv, Vector3D *old);      /* newv = old */

void  SetVec3D (Vector3D *v, Real x, Real y, Real z);     /*  v = (x, y, z)  */

void  SetVec4D (Vector4D *v, Real x, Real y, Real z, Real w);     /*  v = (x, y, z, w)  */

void  AddVec3D (Vector3D *v, Vector3D *v1, Vector3D *v2);      /* v = v1 + v2  */

void  SubVec3D (Vector3D *v, Vector3D *v1, Vector3D *v2);       /* v = v1 - v2  */

void  ScaleVec3D (Vector3D *v, Real coef, Vector3D *v1);   /*  v = coef * v1  */

int   Vec3DOverScale (Vector3D *v, Vector3D *v1, Real dm);  /* v = v1 / dm  */

void  CombVec3D (Vector3D *v, Real c1, Vector3D *v1, Real c2, Vector3D *v2);  /* v = c1*v1 + c2*v2 */

void  DotVec3D (Real *dot, Vector3D *v1, Vector3D *v2);     /*  dot = v1 * v2  */

void  CrossVec3D (Vector3D *v, Vector3D *v1, Vector3D *v2);    /*  v = v1 x v2   */

int   LenVec3D (Real *len, Vector3D *v);         /* len = |v| */

int   DistVec3D (Real *len, Vector3D *v1, Vector3D *v2);

int   UnitVec3D (Vector3D *v);     /* v = v/|v|   */

int   ZeroVec3D (Vector3D *v);    /* Is v zerovector ?  */

int   SameVec3D (Vector3D *v1, Vector3D *v2);    /* v1 == v2 ?  */

int   SamePnt3D (Vector3D *v1, Vector3D *v2);    /* v1 == v2 ?  */

/**********************************************************\
*                                                          *
*          Operations for 4D Vectors                       *
*                                                          *
\**********************************************************/

void  CopyVec4D (Vector4D *newv, Vector4D *old);      /* new = old */

void  AddVec4D (Vector4D *v, Vector4D *v1, Vector4D *v2);     /* v = v1 + v2  */

int  LenVec4D (Real *len, Vector4D *v);

int  UnitVec4D (Vector4D *v);              /* v = v/|v|  */

void  SubVec4D (Vector4D *v, Vector4D *v1, Vector4D *v2);       /* v = v1 - v2  */

void  ScaleVec4D (Vector4D *v, Real coef, Vector4D *v1);   /*  v = coef * v1  */

void  CombVec4D (Vector4D *v, Real c1, Vector4D *v1, Real c2, Vector4D *v2);  /* v = c1*v1 + c2*v2 */

void  Vec3To4 (Vector4D *v4, Vector3D *v3, Real w);  /* v4 = (v3,1)*w */

void  Vec4Trunc3 (Vector3D *v3, Vector4D *v4);  /* v3 = (x, y, z) of v4 */

int   Vec4Prj3 (Vector3D *v3, Vector4D *v4);  /* v3 = (x,y,z)/w of v4 */

/**********************************************************\
*                                                          *
*          Operations for 3D Matrices                      *
*                                                          *
\**********************************************************/

void  InitMtx3D (Matrix3D mtx);    /* mtx = I */

void  ProperView (Matrix3D mv);    /* one matrix for a proper view */

void  CopyMtx3D (Matrix3D newm, Matrix3D old);    /* new = old */

void  MulMtx3D (Matrix3D mtx, Matrix3D mtx1, Matrix3D mtx2);   /* mtx = mtx1 * mtx2 */


/**********************************************************\
*                                                          *
*         Some procedures for transformation               *
*                                                          *
\**********************************************************/

void  ShiftMtx3D (Matrix3D mtx, Real x, Real y, Real z);  /* translate with (x, y, z) */

void  ScaleMtx3D (Matrix3D mtx, Real s);  /* scale with s */
    
void  RotNMtx3D (Matrix3D mtx, Real angle, int n); 

void  RotXMtx3D (Matrix3D mtx, Real Angle);   /* rotate around x axis */

void  RotYMtx3D (Matrix3D mtx, Real Angle);   /* rotate around y axis */

void  RotZMtx3D (Matrix3D mtx, Real Angle);   /* rotate around z axis */

int   TransfVec3D (Vector3D *newv, Vector3D *p, Matrix3D mtx);   /*  new = p*mtx  */

/*
** to compute the inverse matrix of a known matrix
** mti: the inverse matrix 
** mtx: the original matrix 
*/
int   MatrixInv(Matrix2D mti, Matrix2D mtx);
 
/* 
** to make a transform matrix from Local Coordinate
**    System to World Coordinate System
*/
int   LcsToWcs (Matrix3D mtx, Lcs3D *lcs);
 
/*
** to make a transform matrix from World Coordinate
**    System to Local Coordinate System
*/
int   WcsToLcs (Matrix3D mtx, Lcs3D *lcs);


#endif
