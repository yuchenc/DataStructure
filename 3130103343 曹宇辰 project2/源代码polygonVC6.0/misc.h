

#ifndef	MISC_H
#define	MISC_H

#define	SUCCESS		1
#define	TRUE   		1
#define	FAIL		0
#define	FALSE		0

#define  X_AS           0
#define  Y_AS           1
#define  Z_AS		2

#define  LFSD           0
#define  RISD           1


#define  PI           3.14159265
#define  DTOR         0.0174532922222222
#define  RTOD         57.295780490442965

#define  ABS(a)     (((a)<0) ? -(a) : (a))
#define  ZSGN(a)    (((a)<0) ? -1 : ((a)>0 ? 1 : 0))

 
#define  SptEps   0.0001    /* same point tol         */
#define  PatEps   0.001     /* point approximate tol  */
#define  IotEps   0.005     /* intersect optimize tol */
#define  SltEps   0.1       /* subdivision level tol  */
#define  ZeroEps  0.00001   /* zero tolerance         */
 
 
#ifdef	DEBUG
#define	Dprintf(x)		printf (x)
#define	Dprintf1(x)		printf (x)
#define	Dprintf2(x,y)   	printf (x, y)
#define	Dprintf3(x,y,z) 	printf (x,y,z)
#else
#define	Dprintf(x)
#define	Dprintf2(x,y)
#define	Dprintf3(x,y,z)
#endif



#endif


