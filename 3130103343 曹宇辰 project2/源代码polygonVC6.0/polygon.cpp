#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>
#include "misc.h"
#include "vecmtx.h"
#include "polygonprocess.h"


void myinit(void)
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glShadeModel(GL_FLAT);
}

void CALLBACK display(void)
{
	printf("display!\n");
}

typedef struct LNode {
	float x, y, z;
	struct LNode *next;
}
LNode, *LinkList;

LinkList Insert(LinkList &L, int i, float x, float y, float z){
	LNode *p, *s;
	p=L->next;
	int j;
	j=1;
	while(p&&j<i){
		p=p->next;
		j++;
	}
	s=(LinkList)malloc(sizeof(LNode));
	s->x=x;
	s->y=y;
	s->z=z;
	s->next=p->next;
	p->next=s;
	return L;
}

int Delete(LinkList &L, int i){
	LNode *p, *s;
	p=L;
	int j;
	j=1;
	while(p->next&&j<i-1){
		p=p->next;
		++j;
	}
	s=p->next;
	p->next=s->next;
	free(s);
	return true;
}

LinkList Create(Polygon3D *plyg){
	LNode *head, *p, *r;
	int n;
	int i;
	n=plyg->num_pnt;
	head=(LNode *)malloc(sizeof(LNode));
	r=head;
	for(i=0;i<n;i++){
		p=(LNode *)malloc(sizeof(LNode));
		p->x=plyg->Point[i].x;
		p->y=plyg->Point[i].y;
		p->z=plyg->Point[i].z;
		r->next=p;
		r=p;
	}
	r->next=NULL;
	return head;
}

Polygon3D* InsertKnot(Polygon3D *plyg){
	int n;
	int i;
	float x1, x2, y1, y2;
	float xn1, xn2, xn3, yn1, yn2, yn3;
	LinkList L=Create(plyg);
	n=plyg->num_pnt;
	for(i=0;i<n-1;i=i+1){
		x1=(float)plyg->Point[i].x;
		y1=(float)plyg->Point[i].y;
		x2=(float)plyg->Point[i+1].x;
		y2=(float)plyg->Point[i+1].y;
		xn1=x1*2/3+x2/3;
		yn1=y1*2/3+y2/3;
		xn2=(x1+x2)/2+sqrt(3)*(y2-y1)/6;
		yn2=(y1+y2)/2+sqrt(3)*(x1-x2)/6;
		xn3=x1/3+x2*2/3;
		yn3=y1/3+y2*2/3;
		Insert(L,4*i+1,xn1,yn1,0);
		Insert(L,4*i+2,xn2,yn2,0);
		Insert(L,4*i+3,xn3,yn3,0);
	}
	x1=(float)plyg->Point[n-1].x;
	y1=(float)plyg->Point[n-1].y;
	x2=(float)plyg->Point[0].x;
	y2=(float)plyg->Point[0].y;
	xn1=x1*2/3+x2/3;
	yn1=y1*2/3+y2/3;
	xn2=(x1+x2)/2+sqrt(3)*(y2-y1)/6;
	yn2=(y1+y2)/2+sqrt(3)*(x1-x2)/6;
	xn3=x1/3+x2*2/3;
	yn3=y1/3+y2*2/3;
	Insert(L,4*i+1,xn1,yn1,0);
	Insert(L,4*i+2,xn2,yn2,0);
	Insert(L,4*i+3,xn3,yn3,0);
	Point3D *P;
	LNode *p;
	p=L->next;
	n=4*n;
	int n0;
	n0=n<100?n:100;
	if(n0>0){
		P=(Point3D*)malloc(n0*sizeof(Point3D));
	}
	else return (Polygon3D *)NULL;
	for(i=0;i<n0; i++){
		P[i].x=p->x;
		P[i].y=p->y;
		P[i].z=p->z;
		p=p->next;
	}
	if(n>100)
	{
		P=(Point3D *)realloc(P, n*sizeof(Point3D));
		for(i=100;i<n;i++)	{
			P[i].x=p->x;
			P[i].y=p->y;
			P[i].z=p->z;
			p=p->next;
		}
	}
	plyg->num_pnt=n;
	plyg->Point=P;
	return plyg;
}

Polygon3D* DeleteKnot(Polygon3D *plyg, int i){
	int n;
	int j;
	n=plyg->num_pnt;
	LinkList L=Create(plyg);
	Delete(L,i);
	n=n-1;
	Point3D *P;
	LNode *p;
	p=L->next;
	int n0;
	n0=n<100?n:100;
	if (n0>0){
		P=(Point3D *)malloc(n0*sizeof(Point3D));
	}
	else return (Polygon3D *)NULL;
	for(j=0;j<n0;j++){
		P[j].x=p->x;
		P[j].y=p->y;
		P[j].z=p->z;
		p=p->next;
	}
	if(n>100){
		P=(Point3D *)realloc(P, n*sizeof(Point3D));
		for(j=100;j<n;j++){
			P[j].x=p->x;
			P[j].y=p->y;
			P[j].z=p->z;
			p=p->next;
		}
	}
	plyg->num_pnt=n;
	plyg->Point=P;
	return plyg;
}

Polygon3D *Sequence(LinkList P)
{
	Polygon3D *p;
	p=(Polygon3D*)malloc(sizeof(Polygon3D));
	p->closed=1;
	int j,i;
	i=1;
	LNode *r;
	r=P->next;
	while (r->next!=P->next)
	{
		r=r->next;
		i++;
	}
	p->num_pnt=i;
	p->Point=(Point3D*)malloc(sizeof(Point3D)*i);
	Point3D* Pl=p->Point;
	r=P->next;
	for (j=0;j<=i-1;j++)
	{
		Pl[j].x=r->x;
		Pl[j].y=r->y;
		Pl[j].z=r->z;
		r=r->next;
	}
	return p;

}



void pointsdisp(int n, Point3D *Pl)
{
	int i;
	float cx, cy, cz;
	// display a set of points
	glPointSize(5.0);
	
	glBegin(GL_POINTS);
	for (i = 0; i < n; i++) {
		cx = (float)(Pl[i].x);
		cy = (float)(Pl[i].y);
		cz = (float)(Pl[i].z);
		glVertex3f(cx,cy,cz);
	}
	glEnd();
	glFlush();
}

void polygondisp(int n, Point3D *Pl, int closed)
{
	int i;
	float cx, cy, cz;

	//display the polygon
	//glLineWidth(4.0);
	glBegin(GL_LINE_STRIP);
	
	for (i = 0; i < n; i++) {
		cx = (float)(Pl[i].x);
		cy = (float)(Pl[i].y);
		cz = (float)(Pl[i].z);
		glVertex3f(cx,cy,cz);
	}
	if (closed)	{
		cx = (float)(Pl[0].x);
		cy = (float)(Pl[0].y);
		cz = (float)(Pl[0].z);
		glVertex3f(cx,cy,cz);
	}
	glEnd();
	glFlush();
}

void	DisplayPolygon3D(Polygon3D *plyg)
{
	if (plyg->num_pnt<=0)	return;

	int i;
	float cx, cy, cz;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.0, 0.0, 1.0);
	polygondisp(plyg->num_pnt, plyg->Point, plyg->closed);

	glColor3f(1.0, 0.0, 0.0);
	pointsdisp(plyg->num_pnt, plyg->Point);
}

void main(int argc, char** argv)
{
	char filename[12];
    MSG msg;

	if (argc==1)  {
		printf("Please input the data file:\n");
		scanf("%s", filename);
	} 

	Polygon3D *plyg;

	if (argc==1) plyg = (Polygon3D *)InputPolygon(filename);
	else		 plyg = (Polygon3D *)InputPolygon(argv[1]);

	double	t0, t1, time;
	int choice;
	printf("1.ADD or 2.DELETE knot:\n");
	scanf("%d", &choice);
	if(choice==1){
		t0=GetTickCount();
		BoundingcomputationForPolygon(plyg);
		plyg=InsertKnot(plyg);
		t1=GetTickCount();
		time=(t1-t0)/1000.0;
		printf("time=%lf\n",time);
	}
	if(choice==2){
		int number;
		printf("The Number of knot:\n");
		scanf("%d",&number);
		t0=GetTickCount();
		BoundingcomputationForPolygon(plyg);
		plyg=DeleteKnot(plyg,number+1);
		t1=GetTickCount();
		time=(t1-t0)/1000.0;
		printf("time=%lf\n",time);
	}
	auxInitDisplayMode (AUX_SINGLE | AUX_RGBA);
	auxInitPosition (0, 0, 700, 700);
	auxInitWindow ("display polygon");
	myinit();
	while (1) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) == FALSE)
			DisplayPolygon3D(plyg);
		if (GetMessage(&msg, NULL, 0, 0) != TRUE) {
			break;
    } 
    TranslateMessage(&msg);
    DispatchMessage(&msg);
    }
	auxMainLoop(display);
}
