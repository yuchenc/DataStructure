#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>
#include <time.h>
#define VertexNum 200  //无序点数


typedef struct point{
    float x,y,z;
};

//辅助数组
typedef struct {
    int P_num;
	float lowcost;
}closedge;

typedef struct{
     point vexs[VertexNum];
	 float arcs[VertexNum][VertexNum];
	 int vexn,arcn;
}MGraph;

void myinit(void)
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glShadeModel(GL_FLAT);
}

void pointsdisp(point p[])
{
	int i,n = VertexNum;
	float cx, cy, cz;
	// display a set of points
	glPointSize(5.0);
	glBegin(GL_POINTS);
	for (i = 0; i < n; i++) {
		cx = (float)(p[i].x);
		cy = (float)(p[i].y);
		cz = (float)(p[i].z);
		glVertex3f(cx,cy,cz);
	}
	glEnd();
	glFlush();
}   

void linedisp(point p[], int a[VertexNum-1][2])
{
    int i;
	float cx, cy, cz;
    int j,k;
       for(i=0;i<VertexNum-1;i++)
	   {
		   j = a[i][0];
		   k = a[i][1];
		  glBegin(GL_LINE_STRIP);
		  cx = (float)(p[j].x);
		  cy = (float)(p[j].y);
		  cz = (float)(p[j].z);
		  glVertex3f(cx,cy,cz);
		  cx = (float)(p[k].x);
		  cy = (float)(p[k].y);
		  cz = (float)(p[k].z);
		  glVertex3f(cx,cy,cz);
			glEnd();
		
	   }
		glFlush();
}


void InitMG(MGraph *G, point a[])
{
    int i,j;
	float k;
    for(i=0;i<VertexNum;i++)
		G->vexs[i] = a[i];
	for(i=0;i<VertexNum;i++)
		for(j=i;j<VertexNum;j++)
		{
			k = sqrt((a[i].x - a[j].x)*(a[i].x - a[j].x) + (a[i].y - a[j].y)*(a[i].y - a[j].y)); //计算几何距离
			G->arcs[i][j] = k;
			G->arcs[j][i] = k;
		}
		G->vexn = VertexNum;
		G->arcn = VertexNum*(VertexNum-1)/2;
}

int minimum(closedge c[],MGraph *G)
{
   int min = 0;
   double temp = 1000;
   int i,n = G->vexn;
   for(i=0;i<n;i++)
	   if(c[i].lowcost!=0 && c[i].lowcost<temp)
	   { 
		   temp = c[i].lowcost;
		   min = i;
	   }
	   return min;
}



Prim_Draw(MGraph *G, int k,point p[])   //k为初始起点
{
    //辅助数组初始化
	int i,v,n = G->vexn;
	auxInitDisplayMode (AUX_SINGLE | AUX_RGBA);  // initialize the figure
	auxInitPosition (0, 0, 700, 700);  //get the location and size of figure
	auxInitWindow ("spanning tree"); // name it
	myinit();// set color


    float cx, cy, cz;
    int line[VertexNum-1][2];
	closedge c[VertexNum];
	for(v=0;v<n;v++)
		if(v!=k)
		{
		   c[v].P_num = k;
		   c[v].lowcost = G->arcs[k][v];
		}
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	c[k].lowcost = 0; //U = {k};
	for(v=0;v<n;v++)
	{
	     k = minimum(c,G);
		 c[k].lowcost = 0;
		 glColor3f(0.0, 0.0, 1.0);
          glBegin(GL_LINE_STRIP);
		  cx = (float)(p[k].x);
		  cy = (float)(p[k].y);
		  cz = (float)(p[k].z);
		  glVertex3f(cx,cy,cz);
		  cx = (float)(p[c[k].P_num].x);
		  cy = (float)(p[c[k].P_num].y);
		  cz = (float)(p[c[k].P_num].z);
		  glVertex3f(cx,cy,cz);   //draw lines
		  glEnd();
		  	glFlush();

		 for(i=0;i<n;i++)
			 if(G->arcs[k][i]<c[i].lowcost)
			 {
				 c[i].P_num = k;
				 c[i].lowcost = G->arcs[k][i];
			 }

	}
	glColor3f(1.0, 0.0, 0.0);
    pointsdisp(p);  //draw the points set



}




void main()
{

	 int i,j;
	point p[VertexNum];
	srand((unsigned) time(NULL));
	for(i=0;i<VertexNum;i++)
		{
		  p[i].x = 1.0*(rand()%10000-5000)/5000;
		  p[i].y = 1.0*(rand()%10000-5000)/5000;
		  p[i].z =0;
		}
      
	MGraph *G = (MGraph*)malloc(sizeof(MGraph));
    InitMG(G,p);
    Prim_Draw(G,1,p);
	system("pause");
}